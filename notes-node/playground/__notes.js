// console.log('Starting notes.js');

const fs = require('fs');

var fetchNotes = () => {
  // We read all existing notes, so we don't remove everything in the file everytime we add a new note
  // We use try and catch because if the file doesn't exist it will produce an error
  try {
    var notesString = fs.readFileSync('notes-data.json');
    return JSON.parse(notesString);
  }
  catch (e) {
    return [];
  }
};

var saveNotes = (notes) => {
  fs.writeFileSync('notes-data.json', JSON.stringify(notes));
};

var addNote = (title, body) => {
  var notes = fetchNotes();
  var note = {
    title,
    body
  };

  // Check if note already exists
  var duplicateNotes = notes.filter((note) => {
    return note.title === title || note.body === body;
  });

  if(duplicateNotes.length === 0)
  {
    notes.push(note);
    saveNotes(notes);
    return note;
  }
}; // End of addNote function

var getALL = () => {
  return fetchNotes();
};

var getNote = (title) => {
  var notes = fetchNotes();
  var filteredNotes = notes.filter((note) => note.title === title);
  // 0 not found
  // 1 the note we want
  return filteredNotes[0];
};

var removeNote = (title) => {
  var notes = fetchNotes();
  // filter notes, removing the one with title of argument
  var newNotes = notes.filter((note) => {
    return note.title !== title;
  });
  // save new filtered notes
  saveNotes(newNotes);

  return notes.length !== newNotes.length;
};

var logNote = (note) => {
  console.log('---');
  console.log(`Title: ${note.title}`);
  console.log(`Body: ${note.body}`);
};

module.exports = {
  addNote,
  getALL,
  getNote,
  removeNote,
  logNote
};
