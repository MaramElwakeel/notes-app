console.log('Starting app.js');

//Load a built-in module to be able to write into file
const fs = require('fs');  //This tell node that you want to fetch all content of fs module and store them in the fs variable

//To append data to a file we use the following function
//   fs.appendFile(file, data[, options], callback)
//fs.appendFile('greetings.txt', 'Hello World!');

//The above function will produce a warning in terminal, so instead we use the following
//     fs.appendFileSync('message.txt', 'data to append');
//fs.appendFileSync('greetings.txt', 'Hello World!');

//------------------------------------------------------------------------------------------//
//Require a module

const os = require('os');

//The os.userInfo() method returns information about the currently effective user
var user = os.userInfo();
//console.log(user);
//fs.appendFileSync('greetings.txt', 'Hello ' + user.username + '!');
//fs.appendFileSync('greetings.txt', `Hello ${user.username}!`);

//------------------------------------------------------------------------------------------//
//Require our own files

//const path = require('path');
const notes = require('./notes.js');
//fs.appendFileSync('greetings.txt', `You are ${notes.age}.`);

// var result = notes.addNote();
// console.log(result);
//
// var sum = notes.add(-2, -2);
// console.log(sum);

//------------------------------------------------------------------------------------------//
//Require a package you have installed from npm (3rd party library)

const _ = require('lodash');

// console.log(_.isString(true));   //-> false as true is a boolean not a string
// console.log(_.isString('Maram'));  //-> True

var filtered = _.uniq(['Maram', 1,'Elwakeel', 2, 3, 2, 4]); // => ['Maram', 1, 2, 3, 4]
console.log(filtered);
