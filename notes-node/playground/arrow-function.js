// var square = (x) => {
//   var result = x * x;
//   return result;
// };

// var square = (x) => x * x;

var square = x => x * x;
console.log(square(9));

var user = {
  name: 'Maram',
  sayHi: () => {
    console.log(arguments);
    console.log(`Hi. I'm ${this.name}`);
  },
  //The above syntax can't bind this in arrow functions
  sayHiAlt () {
    console.log(arguments);
    console.log(`Hi. I'm ${this.name}`);
  }
};
user.sayHiAlt(1, 2, 3);
